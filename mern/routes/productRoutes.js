const express = require('express');
const router = express.Router();
const { ObjectId } = require('mongodb');
const connectDB = require('../db/dbConnection');


// WSZYSTKIE PRODUKTY
router.get('products', async (req, res) => {
  try {
    const db = await connectDB();
    const products = await db.collection('products').find().toArray();
    res.json(products);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// NOWY PRODUKT
router.post('/products', async (req, res) => {
  const { name, price, quantity } = req.body;

  try {
    const db = await connectDB();

    // CZY ISTNIEJE
    const existingProduct = await db.collection('products').findOne({ name });
    if (existingProduct) {
      return res.status(400).json({ error: 'Product with this name already exists' });
    }

    const newProduct = {
      name,
      price,
      quantity,
    };

    await db.collection('products').insertOne(newProduct);

    res.json(newProduct);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

//EDYTOWANIE PRODUKTU
router.put('/products/:id', async (req, res) => {
  const { name, price, quantity } = req.body;

  try {
    const db = await connectDB();

    const updatedProduct = await db.collection('products').findOneAndUpdate(
      { _id: ObjectId(req.params.id) },
      { $set: { name, price, quantity } },
      { returnDocument: 'after' }
    );

    if (!updatedProduct.value) {
      return res.status(404).json({ error: 'Product not found' });
    }

    res.json(updatedProduct.value);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// USUWANSKO
router.delete('/products/:id', async (req, res) => {
  try {
    const db = await connectDB();

    const deletedProduct = await db.collection('products').findOneAndDelete({
      _id: ObjectId(req.params.id),
    });

    if (!deletedProduct.value) {
      return res.status(404).json({ error: 'Product not found' });
    }

    res.json(deletedProduct.value);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

module.exports = router;