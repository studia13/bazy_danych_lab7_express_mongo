const { MongoClient } = require('mongodb');
require('dotenv').config();

const connectDB = async () => {
  try {
    const client = new MongoClient(process.env.DB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    await client.connect();
    console.log('Connected to MongoDBB');
    return client.db(); // Zwracamy obiekt bazy danych
  } catch (error) {
    console.error('Connection to MongoDB failed:', error.message);
    process.exit(1);
  }
};

module.exports = connectDB;