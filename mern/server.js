const express = require('express');
const { MongoClient } = require('mongodb');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 27017;

const uri = 'mongodb://localhost:27017/magazyn.products';
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

client.connect()
  .then(() => {
    console.log('Connected with MongoDB');
  })
  .catch((err) => {
    console.error('Connection to MongoDB failed:', err);
    process.exit(1); 
  });

app.use(bodyParser.json());
app.use(cors()); 


app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
